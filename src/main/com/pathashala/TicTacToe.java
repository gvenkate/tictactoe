package com.pathashala;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;
import static java.util.stream.Stream.of;

public class TicTacToe {
    private List<List<Integer>> board;

    TicTacToe(List<List<Integer>> board) {
        this.board = board;
    }

    public int winner() {
        Optional<Integer> columnMatch = match(this::allSameInColumn);
        if (columnMatch.isPresent()) {
            return board.get(0).get(columnMatch.get());
        }

        Optional<Integer> rowMatch = match(this::allSameInRow);
        if (rowMatch.isPresent()) {
            return board.get(rowMatch.get()).get(0);
        }

        if (allSameInDiagonal(0, 2)
                || allSameInDiagonal(2, 0)) {
            return board.get(0).get(0);
        }
        return -1;
    }

    private Optional<Integer> match(Predicate<Integer> isSame) {
        return of(0, 1, 2).filter(isSame).findFirst();
    }

    private boolean allSameInRow(int row) {
        return allIdentical(board.get(row));
    }

    private boolean allSameInColumn(int column) {
        return allIdentical(transpose(board).get(column));
    }

    private boolean allIdentical(List<Integer> integers) {
        return integers.stream().distinct().count() == 1;
    }

    private boolean allSameInDiagonal(int diagonalStart, int diagonalEnd) {
        return board.get(0).get(diagonalStart).equals(board.get(1).get(1))
                && board.get(1).get(1).equals(board.get(2).get(diagonalEnd));
    }

    private static List<List<Integer>> transpose(List<List<Integer>> board) {
        return of(0, 1, 2)
                .map(index -> toList(board, index))
                .collect(Collectors.toList());
    }

    private static List<Integer> toList(List<List<Integer>> board, Integer index) {
        return asList(board.get(0).get(index), board.get(1).get(index), board.get(2).get(index));
    }
}
