package com.pathashala;

import org.junit.jupiter.api.Test;

import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class TicTacToeTest {
    @Test
    public void player0WinsWhenFirstRowIsAll0s() {
        List<List<Integer>> board = asList(asList(0, 0, 0),
                                            asList(1, 0, 0),
                                            asList(1, 1, 0));
        int winner = new TicTacToe(board).winner();
        assertEquals(0, winner);
    }

    @Test
    public void player1WinsWhenFirstRowIsAll1s() {
        List<List<Integer>> board = asList(asList(1, 1, 1), asList(1, 0, 0), asList(1, 1, 0));
        int winner = new TicTacToe(board).winner();
        assertEquals(1, winner);
    }

    @Test
    public void player1WinsWhenFirstColumnIsAll1s() {
        List<List<Integer>> board = asList(asList(1, 0, 1), asList(1, 0, 0), asList(1, 1, 0));
        int winner = new TicTacToe(board).winner();
        assertEquals(1, winner);
    }

    @Test
    public void player1WinsWhenSecondColumnIsAll1s() {
        List<List<Integer>> board = asList(asList(0, 1, 1), asList(0, 1, 0), asList(1, 1, 0));
        int winner = new TicTacToe(board).winner();
        assertEquals(1, winner);
    }

    @Test
    public void player1WinsWhenRightToLeftDiagnolIsAll1() {
        List<List<Integer>> board = asList(asList(1, 0, 1), asList(0, 1, 0), asList(0, 1, 1));
        int winner = new TicTacToe(board).winner();
        assertEquals(1, winner);
    }

    @Test
    public void player1WinsWhenLeftToRightDiagnolIsAll1() {
        List<List<Integer>> board = asList(asList(1, 0, 1), asList(0, 1, 0), asList(1, 1, 0));
        int winner = new TicTacToe(board).winner();
        assertEquals(1, winner);
    }

    @Test
    public void noResultWhenNoWinner() {
        List<List<Integer>> board = asList(asList(1, 0, 1), asList(0, 1, 0), asList(0, 1, 0));
        int winner = new TicTacToe(board).winner();
        assertEquals(-1, winner);
    }
}